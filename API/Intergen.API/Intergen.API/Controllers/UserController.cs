﻿using System;
using System.Collections.Generic;
using System.Web.Http;

namespace Intergen.API.Controllers
{
    public class UserController : ApiController
    {

        // GET: api/User
        public IEnumerable<string> Get()
        {
            List<string> list = new List<string>();
            list.Add("Some user");
            return list;
        }

        // GET: api/User/5
        public string Get(Guid id)
        {
            return string.Format("user found with id {0}", id);
        }

        // POST: api/User
        public void Post([FromBody]string user)
        {
            if (string.IsNullOrEmpty(user)) throw new ArgumentNullException("user");
            //Update!
        }

        // PUT: api/User
        public void Put([FromBody]string user)
        {
            if (user == null) throw new ArgumentNullException("user");
            // Add!
        }

        // DELETE: api/User/5
        public void Delete(Guid id)
        {
            // Delete!
        }
    }
}
