﻿using System.Web.Http;

namespace Intergen.API.Controllers
{
    /// <summary>
    /// CompanyNewsController
    /// </summary>
    public class CompanyNewsController : ApiController
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CompanyNewsController"/> class.
        /// GET: api/CompanyNews
        /// </summary>
        public CompanyNewsController()
        {

        }
    }
}
