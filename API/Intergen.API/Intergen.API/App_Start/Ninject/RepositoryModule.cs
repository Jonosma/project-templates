﻿using Ninject.Modules;
using Ninject.Web.Common;

namespace Intergen.API.Ninject
{
    /// <summary>
    /// Repository module
    /// </summary>
    public class RepositoryModule : NinjectModule
    {
        /// <summary>
        /// Loads the module into the kernel.
        /// </summary>
        public override void Load()
        {

        }
    }
}