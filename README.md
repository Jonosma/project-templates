<h2>Project templates for Web and API projects</h2>
<br />
<h3>Web project includes</h3>
	- Ninject
	- Underscore
 	- Angular
	- Kendo
<br />
<h3>API project includes</h3>
	- Ninject

Note: when pulling down the project files, you will need to enable nuget package restore.

To install the templates - copy the zip file to your templates directory e.g. C:\Users\jonathanc\Documents\Visual Studio 2013\Templates\ProjectTemplates