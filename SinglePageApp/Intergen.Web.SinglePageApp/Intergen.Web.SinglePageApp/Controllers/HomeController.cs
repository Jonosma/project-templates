﻿using System.Web.Mvc;

namespace Intergen.Web.SinglePageApp.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}