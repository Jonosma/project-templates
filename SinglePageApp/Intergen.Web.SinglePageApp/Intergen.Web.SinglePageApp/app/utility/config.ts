﻿ (() => {
     'use strict';
     app.config($routeProvider => {
         $routeProvider.when(Utility.Urls.root, {
             templateUrl: Utility.Templates.users
         }).otherwise({
             redirectTo: Utility.Urls.root
         });
     });
 })();