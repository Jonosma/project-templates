﻿ module Utility {
     'use strict';
     export class Resources {
         public static appName: string = 'CompetenzPortal';
     }

     export class Urls {
         public static root: string = '/';
     }

     export class Templates {
         public static users: string = 'Templates/users.html';
     }
 }