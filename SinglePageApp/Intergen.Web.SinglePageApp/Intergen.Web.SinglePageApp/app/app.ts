﻿/// <reference path="utility/constants.ts" />
/// <reference path="../Scripts/typings/angularjs/angular.d.ts" />
var app;
(() => {
    'use strict';
    app = angular.module(Utility.Resources.appName, ['ngRoute', 'kendo.directives']);
})();