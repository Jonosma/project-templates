﻿
using Ninject.Modules;

namespace Intergen.Web.SinglePageApp.Ninject
{
    /// <summary>
    /// Core Module
    /// </summary>
    public class CoreModule : NinjectModule
    {
        /// <summary>
        /// Loads the module into the kernel.
        /// </summary>
        public override void Load()
        {

        }
    }
}