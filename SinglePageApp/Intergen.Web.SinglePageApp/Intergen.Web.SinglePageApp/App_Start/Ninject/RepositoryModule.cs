﻿using Ninject.Modules;

namespace Intergen.Web.SinglePageApp.Ninject
{
    /// <summary>
    /// Repository module
    /// </summary>
    public class RepositoryModule : NinjectModule
    {
        /// <summary>
        /// Loads the module into the kernel.
        /// </summary>
        public override void Load()
        {
        }
    }
}