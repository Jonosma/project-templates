using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Intergen.Web.SinglePageApp.Ninject;
using Microsoft.Web.Infrastructure.DynamicModuleHelper;
using Ninject;
using Ninject.Modules;
using Ninject.Web.Common;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(Intergen.Web.SinglePageApp.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(Intergen.Web.SinglePageApp.App_Start.NinjectWebCommon), "Stop")]

namespace Intergen.Web.SinglePageApp.App_Start
{
    /// <summary>
    /// Ninject
    /// </summary>
    public static class NinjectWebCommon
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Gets the bootstrapper.
        /// </summary>
        /// <value>
        /// The bootstrapper.
        /// </value>
        public static Bootstrapper Bootstrapper { get { return bootstrapper; } }


        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start()
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }

        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }

        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel(CreateModules().ToArray());
            try
            {
                GlobalConfiguration.Configuration.DependencyResolver = new LocalNinjectDependencyResolver(kernel);
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        private static IEnumerable<INinjectModule> CreateModules()
        {
            yield return new CoreModule();
            yield return new RepositoryModule();
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "kernel")]
        private static void RegisterServices(IKernel kernel)
        {

        }
    }
}
